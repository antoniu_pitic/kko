
package javaapplication5;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


class Admitere {
    List <Student> students = new ArrayList<>();
    
    
    void AddStudent(Student student) {
        students.add(student);
    }

    void ShowAccepted(int noOfStudents) {
        
        Collections.sort(students, (s1, s2) -> {return s2.grade - s1.grade; });
        
        students.subList(0, noOfStudents)
                .forEach((s)->{s.ConsoleShow();});
    }
    
}
