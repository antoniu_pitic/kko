
package javaapplication5;

public class Student {
    String name;
    int grade;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }
    
    public Student(String name, int grade) {
        this.name = name;
        this.grade = grade;
    }

    void ConsoleShow() {
        System.out.println(name + "-" + grade);
    }

}
